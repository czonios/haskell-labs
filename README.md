# Principles of Programming Languages #

## Haskell labs ##
  - [x] Lab01
  - [x] Lab02

## Setting up ##

### Dependencies ###

* Hugs interpreter

    Ubuntu/Debian based Linux distributions:
    `sudo apt-get install hugs`
   
    Arch based Linux distributions:
    `sudo pacman -S hugs`


### Loading the files in hugs ###

* :load command

    `:l LabX.hs`

### Exiting hugs ###

* :quit command

    `:q`