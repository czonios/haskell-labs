
-----------------------------------------------------------------------------------------

-- ASKHSH 1


grade :: Int->Int->Int

grade a b
  | ((a > 100) || (a < 0) || (b > 20) || (b < 0)) = -1
  | ((c > 47) && (a <= 47)) = 47
  | ((c > 47) && (a > 47) && (c < 50)) = 50
  | otherwise = c
  where c = (((8*a) `div` 10) + b)





-----------------------------------------------------------------------------------------

-- ASKHSH 2

counter :: Int->Int->Int

counter a b
  | a <= 9
       = same
  | otherwise
        = counter (a `div` 10) (b `div` 10) + counter (a `mod` 10) (b `mod` 10)
  where same
          | (a == b) = 1
          | otherwise = 0



digits :: Int->Int->Int

digits x y
  | (count == 8) = 1000000
  | (count == 7) = 100000
  | (count == 6) = 8000
  | (count == 5) = 300
  | (count == 4) = 20
  | (count == 3) = 5
  | (count == 2) = 1
  | otherwise = 0
  where count = counter x y








-----------------------------------------------------------------------------------------

-- ASKHSH 3

nset :: Integer->Integer->Integer->Integer->Integer

nset a k m n
  | c < d = n
  | otherwise = nset a k m (n+1)
  where c = ((n + a)^k)
        d = m^n

search :: Integer->Integer->Integer->Integer

search a k m = nset a k m 1










-----------------------------------------------------------------------------------------

-- ASKHSH 4

increment :: Integer->Integer->Integer->Integer

increment m n i
  | (i > n) = 0
  | otherwise = (m + i)^n + increment m n (i+1)


sum2017 :: Integer->Integer->Integer

sum2017 m n = increment m n m








-------------------------------------------------------------------------------------
