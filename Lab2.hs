
-----------------------------------------------------------------------------------------

-- ASKHSH 1

-- checks if x exists in given list
isInList :: Int->[Int]->Bool

isInList x (h:t) = x == h || isInList x t
isInList x [] = False


-- deletes x from given list
deleteX :: Int->[Int]->[Int]

deleteX x (h:t)
  | x == h
    = t
  | otherwise
    = h : deleteX x t

deleteX x [] = []


update :: Int->[Int]->[Int]

update n s
  | isInList n s
    = update n (deleteX n s)
  | otherwise
    = s ++ [n]
update n [] = [n]








-----------------------------------------------------------------------------------------

-- ASKHSH 2

--ckecks for negative i, j
negativei :: Int->Int

negativei x
  | x < 0
    = 1
  | x >= 0
    = x

fromTo :: Int->Int->[u]->[u]

fromTo i j s = cutlist2
  where
    x = negativei i
    y = negativei j
    (temp, cutlist) = splitAt (x-1) s
    (cutlist2, temp2) = splitAt (j-x+1) cutlist




-----------------------------------------------------------------------------------------

-- ASKHSH 3
sumf :: (Int->Int)->Int->Int->Int
sumf f x n
  | x == -k
    = f x
  | otherwise
    = (sumf f (x-1) n) + (f x)
  where k = (abs n)


hosum :: (Int->Int)->(Int->Int)


hosum f = \x -> (sumf f (abs x) x)







-----------------------------------------------------------------------------------------

-- ASKHSH 4
insSort :: Ord u => [u]->[u]
insSort (h:t) = insert h (insSort t)
insSort [] = []

insert :: Ord u => u->[u]->[u]
insert n (h:t)
  | n < h
    = n : h : t
  | n == h
    = h:t
  | otherwise
    = h : insert n t
insert n [] = [n]


apply1 :: Ord u => [v->u]->[v]->[u]

apply1 p [] = []
apply1 [] s = []
apply1 [] [] = []
apply1 (h:[]) (f:[]) = [h f]
apply1 (h:t) (f:r) = (apply [h] [f]) ++ (apply t [f])
                    ++ (apply [h] r) ++ (apply t r)


apply :: Ord u => [v->u]->[v]->[u]

apply p s = (insSort (apply1 p s))
